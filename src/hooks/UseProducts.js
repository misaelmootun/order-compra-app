import { useState } from "react"
let inicialSatete ={
    ordenes:[],
    producto:{id:"", sku:'',name:'',quantity:'',price:''},
    listaproducto: localStorage.getItem("productos") ? JSON.parse(localStorage.getItem("productos")) : [],
    errores :{},
    estatus:"espera",
};

const useProducs = (state=inicialSatete) => {
     const [productos,setState]= useState(state);    
     const getProductos = (id,nuevo) => {

        if (nuevo){
            const ordenes = JSON.parse(localStorage.getItem("ordenes"));
            const produ = ordenes.find((item) =>  item.id ===id);
            if (produ.items.length>0){
                setState({...productos, listaproducto:[...produ.items],estatus:"espera" });
                localStorage.setItem("productos",JSON.stringify(produ.items));
            }
        }else{
            setState({...productos,estatus:"espera" });
        }
    }

    const setForm =({target})=>{
         
        setState({...productos,producto:{ ...productos.producto, [target.id]:target.value }});
     }

     const  guardarProducto = (producto) => {
        let status =true;  
        let errores ={};     
        producto.id = new Date().getUTCMilliseconds(); 
        if (producto.sku.trim() === ""){
            status =false;
            
            errores ={"sku" : ["sku obligatorio"]};
        } 
        else if (producto.name.trim() === ""){
            errores ={"name" : ["sku obligatorio"]};
            status =false;
        }
        else if (producto.quantity.trim() === ""){
            errores ={"quantity" : ["sku obligatorio"]};
            status =false;
        }
        else if (producto.price.trim() === ""){
            errores ={"price" : ["sku obligatorio"]};
            status =false;
        }

        if (status){
            let prod =[...productos.listaproducto,producto];
            setState({...productos, listaproducto:prod,estatus:"ok",errores:{},producto:{id:"", sku:'',name:'',quantity:'',price:''} });
            localStorage.setItem("productos",JSON.stringify(prod));
        }else{
            setState({...productos, estatus:"errores",errores:errores });
        }
        
    }


    return {
        productos,
        getProductos,
        setForm,
        guardarProducto
    }

}

export default useProducs;