import React from "react";
import { Form } from "react-bootstrap";

const FormProduct = ({producto,setForm,errs}) => {

     const {sku,name,quantity,price} =producto;
    return <>         
      <Form>
        <Form.Group className="mb-3" controlId="sku">
            <Form.Label>SKU</Form.Label>
            <Form.Control   type="text" onChange={(e)=>{setForm(e);}} value={sku} required isInvalid={errs.sku ? true : false}  placeholder="Sku" />
             { errs.sku ? <label>Campo obligatorio</label>: '' }
        </Form.Group>
        <Form.Group className="mb-3"  controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control name="name" value={name} onChange={(e)=>{setForm(e);}} required isInvalid={errs.name ? true : false}  type="text" placeholder="Name" />
            { errs.name ? <label>Campo obligatorio</label>: '' }
        </Form.Group>
        <Form.Group className="mb-3" controlId="quantity">
            <Form.Label>Quantity</Form.Label>
            <Form.Control  value={quantity} onChange={(e)=>{setForm(e);}} type="number" required isInvalid={errs.quantity ? true : false}  placeholder="Quantity" />
            { errs.quantity ? <label>Campo obligatorio</label>: '' }
        </Form.Group>
        <Form.Group className="mb-3" controlId="price">
            <Form.Label>Price</Form.Label>
            <Form.Control type="number"  onChange={(e)=>{setForm(e);}} required isInvalid={errs.price ? true : false}  value={price} placeholder="Price"  />
            { errs.price ? <label>Campo obligatorio</label>: '' }
        </Form.Group>
    </Form>

   
    </>
}

export default FormProduct;