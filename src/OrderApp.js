import React from "react";
import TableOrdenes from "./components/TableOrdenes";
import TableProductos from "./components/TableProductos";
import Menu from "./menu/Menu";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

const OrderApp = () => {

    return (
        <>
         <BrowserRouter>
         <Routes>
             <Route path="/" element={ <Menu /> }>
                <Route path="ordenes" element={<TableOrdenes />} />
                <Route path="productos/:id"  element={<TableProductos />} />
                <Route path="/"  element={<TableOrdenes />} />
             </Route>
            
        </Routes>
         </BrowserRouter>
        </>  
    );
}

export default OrderApp;