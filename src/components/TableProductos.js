import React, { useEffect,useState } from "react";
import { Col, Row, Table,Modal,Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import FormProduct from "./FormProduct";
import useProducs from "../hooks/UseProducts";
import { ToastContainer, toast } from 'react-toastify';
const TableProductos = () => {
     
    const { id } = useParams(); 
    const {productos,getProductos,setForm,guardarProducto} =  useProducs();
    const {producto,listaproducto,errores,estatus } =productos;
    const [show, setShow] = useState(false);
/* eslint-disable */
     useEffect(()=>{
            getProductos(id,true);
     },[]);
     useEffect(()=>{
         if (estatus==="ok"){
            getProductos(id,false);
            handleClose();
            toast.success("Información guardada con éxito");
         }
/* eslint-disable */
 },[estatus]);

     const handleClose = () => setShow(false);
     const handleShow = () => setShow(true);

     const guardar = () => {
         guardarProducto(producto);
     }

    return (
        <>
        
    <Row className="mb-3">
        <Col xs={9}></Col>
        <Col xs={3}>  
        <Button variant="primary" onClick={handleShow}>
            Agregar producto
        </Button> 
        </Col>
        </Row>

        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Nuevo Producto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <FormProduct
            producto={producto}
            setForm={setForm}
            errs={errores}
            />

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={guardar}>
            Guardar
          </Button>
        </Modal.Footer>
      </Modal>

      <ToastContainer />

        <Table striped bordered hover>
            <thead>
                <tr>
                <th>SKU</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {
                    listaproducto.map((item)=> (
                        <tr key={item.id} >
                        <td>{item.sku}</td>
                        <td>{item.name}</td>
                        <td>{item.quantity}</td>
                        <td>{item.price}</td>
                        </tr>
                        )
                    )
                }
            </tbody>
            </Table>
        </>
    )
}

export default TableProductos;