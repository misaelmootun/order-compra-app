
import axios from 'axios';


let getConeccion =()=>{
        let AUTH_TOKEN= "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJwUGFINU55VXRxTUkzMDZtajdZVHdHV3JIZE81cWxmaCIsImlhdCI6MTYyMDY2Mjk4NjIwM30.lhfzSXW9_TC67SdDKyDbMOYiYsKuSk6bG6XDE1wz2OL4Tq0Og9NbLMhb0LUtmrgzfWiTrqAFfnPldd8QzWvgVQ";
        const baseApi =axios.create({
            baseURL:'https://eshop-deve.herokuapp.com/api/v2/'
            
        });
        baseApi.defaults.headers.common['Authorization'] = AUTH_TOKEN;
       
        baseApi.interceptors.response.use((response) => {
          
            return response
          }, async function (error) {
              let {data,status} =error.response;
            if (status === 404 && data.status==='token expirado') {
                window.location.reload();
                alert("Token expirado");
            
            }
            return Promise.reject(error);
          });

        return baseApi;
}




export default getConeccion;