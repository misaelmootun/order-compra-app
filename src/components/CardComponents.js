import React from "react";
import PropTypes from 'prop-types';
import { Row,Button,Col,Card,Badge } from "react-bootstrap";
import { Link } from "react-router-dom";
const CardComponents = ({ item,pagar }) => {
  return (
    <>
    <Card
    bg={ "Info"}
    key={item.id}
    text={"Order2"}
    className="mt-2"
  >
    <Card.Header className={ item.payment.status ==="pay" ? "ClasHeader" : "" }>Orden de compra # {item.number}</Card.Header>
    <Card.Body>
    <Row>
      <Col>
      <strong> Subtotal: </strong> <Badge pill bg="primary">
              $ {item.totals.subtotal}
          </Badge>
      </Col>
      <Col>
      <strong> Total:</strong>  <Badge pill bg="primary">
       $ {item.totals.total}
        </Badge>
      </Col>
     </Row>
     
      <Row className="mt-3">
          <Col>
          <Link variant="info"  size="sm" className="ml-4" to={`/productos/${item.id} `} > Ver mas...</Link>    
          </Col>
          <Col>
          {item.payment.status ==="pay" ? '' : <Button variant="warning"  onClick={()=>{pagar(item.id)}}  size="sm" className="ml-4">Pagar</Button>}
          </Col>
      </Row>  
    </Card.Body>
  </Card>
    </>
  )
}
CardComponents.propTypes = {
  item: PropTypes.object.isRequired,
  pagar:PropTypes.func.isRequired

};

export default CardComponents;