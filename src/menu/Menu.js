
import React from "react";
import { Navbar,Container,NavDropdown,Nav } from "react-bootstrap"; 
import { Outlet } from "react-router-dom";

const Menu = () => {
    return (
        <>
        <Navbar bg="dark" expand="lg" variant="dark">
            <Container>
                <Navbar.Brand href="/ordenes">Orden de compra</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <NavDropdown title="Configuración" id="basic-nav-dropdown">
                    <NavDropdown.Item href="#">Perfil</NavDropdown.Item>
                    <NavDropdown.Item href="#">Cerrar sesión</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        <Container className="mt-3">
            <Outlet />
        </Container>
        </>
    )
}

export default Menu;