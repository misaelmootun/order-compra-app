
import React,{useEffect} from "react";
import { Row,Col } from "react-bootstrap";
import useOrders from "../hooks/UseOrders";
import CardComponents from "./CardComponents";
import { ToastContainer, toast } from 'react-toastify';

const TableOrdenes =() => {
      const {ordenes,getOrders,pagarOrder} = useOrders();
/* eslint-disable */
      useEffect(()=>{
        getOrders();
     },[]);
/* eslint-disable */
     const pagar = (id) => {
       pagarOrder(id);
        toast.success("El pago fue exitoso");
     }

return (<>
<ToastContainer />
  <Row> 
    {
      ordenes.map((item) => (
        <Col  key={item.id} xs={3}> 
        <CardComponents
            item={item}
            pagar={pagar}
        />
        </Col>
  ))}
  </Row>
 </>)
}

export default TableOrdenes;