import { useState } from "react";
import getConeccion from "../helpers/baseApi"

let inicialSatete= localStorage.getItem("ordenes") ? JSON.parse(localStorage.getItem("ordenes")) : [];

const useOrders =(state=inicialSatete) => {

     const [ordenes,setState] = useState(state);
     
     const getOrders = () => {
        getConeccion().get('orders')
            .then(function ({data:{orders}}) {
                localStorage.setItem("ordenes",JSON.stringify(orders));
             setState(orders);
            })
            .catch(function (error) {
         })
    }

    const pagarOrder = (id) => {
      let newOrden=  ordenes.map((elements)=>{
          let pay =elements.payment.status;
            if (id===elements.id){
                pay="pay";
            }
            return {
                ...elements,
                payment:{
                    status: pay
                }
            }
        });
        localStorage.setItem("ordenes",JSON.stringify(newOrden));
        setState(newOrden);

    }

    return {
        ordenes,
        getOrders,
        pagarOrder
    }

}

export default useOrders;